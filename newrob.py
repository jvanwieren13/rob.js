#!/usr/bin/python

from PIL import Image, ImageOps, ImageFont, ImageDraw
import math
from glob import glob
import random as rnd

files = glob("ims/*jpg")
f = rnd.choice(files)

im = Image.open(f)

class WaveDeformer:

    def transform(self, x, y):
        y = y + 10*math.sin(x/40)
        return x, y

    def transform_rectangle(self, x0, y0, x1, y1):
        return (*self.transform(x0, y0),
                *self.transform(x0, y1),
                *self.transform(x1, y1),
                *self.transform(x1, y0),
                )

    def getmesh(self, img):
        self.w, self.h = img.size
        gridspace = 20

        target_grid = []
        for x in range(0, self.w, gridspace):
            for y in range(0, self.h, gridspace):
                target_grid.append((x, y, x + gridspace, y + gridspace))

        source_grid = [self.transform_rectangle(*rect) for rect in target_grid]

        return [t for t in zip(target_grid, source_grid)]


def random_op(im, op, prob, *args):
    if rnd.random() > prob:
        return op(im, *args)
    return im

deformer = WaveDeformer()

if rnd.random() > 0.7:
    im = ImageOps.crop(im, rnd.randint(0, 200))
if rnd.random() > 0.5:
    im = ImageOps.mirror(im)

ops = [(ImageOps.autocontrast, 0.7, rnd.randint(0, 59)), 
       (ImageOps.invert, 0.76),
       (ImageOps.invert, 0.9),
       (ImageOps.deform, 0.72, deformer),
       (ImageOps.solarize, 0.62, rnd.randint(0, 159)),
       (ImageOps.posterize, 0.62, rnd.randint(1,6)),
       (ImageOps.grayscale, 0.88)]

rnd.shuffle(ops)

for o in ops:
    im = random_op(im, *o)

if rnd.random() > 0.4:
    font = ImageFont.truetype("/usr/share/fonts/TTF/" +
                              rnd.choice(["Monaco_Linux.ttf", "AlteHaasGroteskBold.ttf",
                                          "impact.ttf",  "Symbola.ttf"]), 
                              rnd.randint(69, 260)) # fontsize
    draw = ImageDraw.Draw(im)
    color = None
    while not color:
        try:
            color = (rnd.randint(1, 254), rnd.randint(1, 254), rnd.randint(1, 254))
            draw.text( (rnd.randint(0, im.size[0]//2), rnd.randint(0, im.size[1]//4)), 
                       rnd.choice(["welkom in\n de kelder", "schrobben", "poetsen", "rob geus", "marit", "danny"]),
                       color, font=font)
        except:
            continue
        break

try:
    if rnd.random() > 0.5:
        im = ImageOps.scale(im, 1+rnd.random(), resample=rnd.choice([Image.NEAREST, Image.BOX]))
except:
    pass


im.save("r.jpg", quality=rnd.randint(1, 70))

