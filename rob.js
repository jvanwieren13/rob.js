#!/bin/node

const qrcode = require('qrcode-terminal');
const chrono = require('chrono-node');
const util = require('util');
var cmd=require('node-cmd');

// whatsapp client (you might need the multidevice branch)
const { Client } = require('whatsapp-web.js');
const client = new Client({ puppeteer: { headless: true }, clientId: 'rijnder' });
const { MessageMedia } = require('whatsapp-web.js');

let users = ['danny', 'ml', 'rijnder', 'marit', 'joris'];
let tasks = ['afnemen', 'stofzuigen', 'WCs', 'badkamer', 'vuilnis'];

function choice(choices) {
  var index = Math.floor(Math.random() * choices.length);
  return choices[index];
}

function arrayRotate(arr, count) {
  count -= arr.length * Math.floor(count / arr.length);
  arr.push.apply(arr, arr.splice(0, count));
  return arr;
}

Date.prototype.getWeek = function (dowOffset) {
    dowOffset = typeof(dowOffset) == 'number' ? dowOffset : 0; //default dowOffset to zero
    var newYear = new Date(this.getFullYear(),0,1);
    var day = newYear.getDay() - dowOffset; //the day of week the year begins on
    day = (day >= 0 ? day : day + 7);
    var daynum = Math.floor((this.getTime() - newYear.getTime() - 
    (this.getTimezoneOffset()-newYear.getTimezoneOffset())*60000)/86400000) + 1;
    var weeknum;
    //if the year starts before the middle of a week
    if(day < 4) {
        weeknum = Math.floor((daynum+day-1)/7) + 1;
        if(weeknum > 52) {
            nYear = new Date(this.getFullYear() + 1,0,1);
            nday = nYear.getDay() - dowOffset;
            nday = nday >= 0 ? nday : nday + 7;
            /*if the next year starts before the middle of
              the week, it is week #1 of that year*/
            weeknum = nday < 4 ? 1 : 53;
        }
    }
    else {
        weeknum = Math.floor((daynum+day-1)/7);
    }
    return weeknum;
};

function task(user, date) {
    const u = user.toLowerCase();
    const n = tasks.length;
    const r = arrayRotate([...tasks], (date.getWeek() % n)*-1);
    // users and tasks are a mapping
    const idx = users.indexOf(u);
    return r[idx];
}

function checkdate(txt) {
    // make sure date starts on monday
    const l = chrono.nl.parse(txt);
    if (l.length == 0) {
        return chrono.nl.parse('vandaag')[0]; 
    }
    else {
        return l[0];
    }
}

function progressTable() {
    const d = new Date();
    var t = '';
    var t_header = d.toLocaleDateString() + "\n";
    t_header += "———————————————————\n";
    row = "%s\t\t%s\t\t%s\n";

    for (var u of users) {
        // TODO: get finished from file
        var n;
        if (u === "ml") {
            n = "*ML*:     "
        }
        else {
            n = "*"+u[0].toUpperCase()+u.slice(1)+"*:";
        }
        t += util.format(row, n, task(u, d), choice(["✅", "❌"]));
    }
    return t_header + t;
}

function roblogic(txt, sender) {
    const d = checkdate(txt);
    let header = ``;
    var reply;

    if (users.some(u => txt.includes(u))) {
        for (var u of users) {
            if (txt.includes(u)) {
                break;
            }
        }
        // if today
        if (d.date().toDateString() == new Date().toDateString()) {
            reply = task(u, d.date());
        }
        else {
            var t = task(u, d.date());
            var name = u[0].toUpperCase() + u.slice(1);
            reply = d.text + " moet " + name + " " + t;
        }
    }
    else if (['✅', 'genomen', 'done', 'gedaan'].some(t => txt.includes(t))) {
        if (Math.random() > 0.4) {
            reply = sender+": " + task(sender, new Date()) + " ✅";
        }
        else {
            reply = "moet ik nu trots op je zijn?";
        }
    }
    else if (txt.includes('ik')) {
        if (d.date().toDateString() == new Date().toDateString()) {
            reply = task(sender, d.date());
        }
        else {
            const t = task(sender, d.date());
            reply = d.text + " moet je " + t;
        }
    }
    else if (txt.includes('help')) {
    if (txt.endswith('afnemen')) {
        reply = "Fornuis, keukenblad, tafel in de woonkamer, vensterbanken, schoenenbakkie strelen";
    }
    if (txt.endswith('stofzuigen')) {
        reply = "Als je boven niet doet kan je het net zo goed niet doen";
    }
    if (txt.endswith('wc')) {
        reply = "Het is vrij ranzig maar pak alles, ook onder de bril enzo";
    }
    if (txt.endswith('badkamer')) {
        reply = "Een dag zonder chloor is een dag niet geleefd, gebruik ontkalker voor de kranen mochten die vies zijn. Klop de badmat uit/doe hem in de was. Doe ook de doekjes enzo even wassen chillll";
    }
    if (txt.endswith('vuilnis')) {
        reply = "Vergeet niet het glas of het karton te verwerken!";
    }
    else {   reply = "Jij valt niet te redden";
    }
    }
    else if (txt.includes('plaatje')) {
        reply = "";
    }
    else if (txt.includes('kong')) {
        reply = "🍌";
    }
    else if (txt.includes('toekabaap')) {
        reply = "kief kief";
    }
    else { // assume status/progress report
        reply = progressTable();
    }

    var footer = '';
    if (Math.random() > 0.15) {
        footer = "\n";
        footer += choice(["- liefs, rob", "- rob 💖", "- je boy (Rob Geus)", 
        "💝 Mr. Geus", "- Alles wel zo schoon, Rob Geus 💦", 
        "- Straks, _Red Mijn Vakantie!_, op RTL5",
        "- Kijk nu het nieuwe seizoen van _De Smaakpolitie_ op SBS6, 19:30"]);
    }

    return header + reply + footer;
}


client.on('authenticated', () => {
    console.log('Authenticated');
});

client.on('auth_failure', msg => {
    // Fired if session restore was unsuccessfull
    console.error('Authentication failure', msg);
});

client.on('change_state', state => {
    console.log('CHANGE STATE', state );
});

client.on('disconnected', (reason) => {
    console.log('Client was logged out', reason);
});

client.on('qr', qr => {
    qrcode.generate(qr, {small: true});
});

client.on('ready', () => {
    console.log('Client is ready!');
});


client.on('message_create', async msg => {
    // move below "includes()"
    const txt = msg.body.toLowerCase();
    var sender;
    if (msg.fromMe) {
        sender = "rijnder"
    }
    else {
        const contact = await msg.getContact();
        sender = contact.pushname;
        if (contact.pushname === undefined) {
            sender = contact.name;
        }
    }
    console.log("sender: " + sender);
    if (txt.includes('@rob')) {
        const r = roblogic(txt, sender);
        // get picture
        // send messages
        msg.reply(r);
        if (Math.random() > 0.2 || txt.includes("plaatje")) {
            cmd.runSync("./newrob.py");
            const media = MessageMedia.fromFilePath("./r.jpg");
            const c = await msg.getChat();
            c.sendMessage(media);
        }
    }
});

function test() {
    var sender = "rijnder";
    var readline = require('readline');
    var rl = readline.createInterface(process.stdin, process.stdout);

    rl.setPrompt('rob> ');
    rl.prompt();

    rl.on('line', function(a) {
        const r = roblogic(a.trim().toLowerCase(), sender);
        console.log(r);
        rl.prompt();
    }).on('close', function() {
         process.exit(0);
    });
}

const optionDefinitions = [
  { name: 'verbose', alias: 'v', type: Boolean },
  { name: 'test', alias: 't', type: Boolean },
  { name: 'help', alias: 'h', type: Boolean }
]

const commandLineArgs = require('command-line-args')
const options = commandLineArgs(optionDefinitions)

if (options.help) {
    console.log(`usage: rob.js [-t|--test] \n
-t:	Run rob in offline mode
-h:	Generate this help message`)
    process.exit(0)
}
else if (options.test) {
    test();
}
else {
    client.initialize().catch(console.log);
}
